import * as firebase from "firebase/app"

import "firebase/auth"
import "firebase/database"

var firebaseConfig = {
	apiKey: "AIzaSyD7hXaQiKB04WS_GZ-aNBdZAAWIZMnCesQ",
	authDomain: "practice-f6be4.firebaseapp.com",
	databaseURL: "https://practice-f6be4.firebaseio.com",
	projectId: "practice-f6be4",
	storageBucket: "practice-f6be4.appspot.com",
	messagingSenderId: "436399993946",
	appId: "1:436399993946:web:ad7709d26178d1ad7b5463"
}

let firebaseApp = firebase.initializeApp(firebaseConfig)
let firebaseAuth = firebaseApp.auth()
let firebaseDb = firebaseApp.database()

export { firebaseAuth, firebaseDb }