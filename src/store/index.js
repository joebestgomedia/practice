import Vue from 'vue'
import Vuex from 'vuex'

import lists from './lists'
import auth from './auth'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      lists,
      auth
    },
    strict: process.env.DEV
  })

  return Store
}
