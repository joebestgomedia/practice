import { LocalStorage } from 'quasar'
import { firebaseAuth } from 'boot/firebase'

const state = {
	loggedIn: false

}

const mutations = {
	setLoggedIn(state, value) {
		state.loggedIn = value
	}
}

const actions = {
	registerUser({}, payload) {
		firebaseAuth.createUserWithEmailAndPassword(payload.email, payload.password)
		.then(response => {
			console.log('response: ', response)
		})
		.catch(error => {
		console.log('error .message: ', error.message)
	})
	},
	loginUser({dispatch}, payload) {
		firebaseAuth.signInWithEmailAndPassword(payload.email, payload.password)
		.then(response => {
			console.log('response: ', response)
			dispatch('lists/logData', 'Logged in', {root:true})
		})
		.catch(error => {
		console.log('error .message: ', error.message)
	})
	},
	logoutUser ({dispatch}) {
		dispatch('lists/logData', 'Logged out', {root:true})
		firebaseAuth.signOut()
	},



  handleAuthStateChange ({ commit, dispatch }) {
    firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        commit('setLoggedIn', true)
        LocalStorage.set('loggedIn', true)
        this.$router.push('/')
        dispatch('lists/fbReadData', null, {root:true})
      } else {
      	commit('lists/clearItems', null, { root:true })
        commit('setLoggedIn', false)
        LocalStorage.set('loggedIn', false)
        this.$router.replace('/login')
      }
    })
  }

}

const getters = {

}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}