import Vue from 'vue'
import { uid } from 'quasar'
import { firebaseDb, firebaseAuth } from 'boot/firebase'

const state = {
  lists: 
  {
    // id: 1,
    // name:'bob'
  }

}

const mutations = {
  addItem(state, payload){
    Vue.set(state.lists, payload.id, payload.list)
  },
  clearItems(state) {
    console.log('clear ')
    state.lists={}
  }
}

const actions = {
  logData({}, message){
    let userId = firebaseAuth.currentUser.uid
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()+ ":" + today.getMilliseconds();
    let payload = { time: time, log: message}
    let logRef = firebaseDb.ref('log/'+userId+'/'+payload.time)
    logRef.set(payload.log)
  },

  fbReadData({ commit, dispatch }) {
    commit('clearItems')

    console.log('start reading data')
    let userId = firebaseAuth.currentUser.uid
    let userItems = firebaseDb.ref('practice/'+userId)

    userItems.on('child_added', snapshot => {
      let list = snapshot.val()
      let payload = {
        id: snapshot.key,
        list: list
      }
      dispatch('logData', "Retrived data from database")
      commit('addItem', payload)
    })
  }
}

const getters = {
  lists: (state) => {
    return state.lists
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
